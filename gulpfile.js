// including plugins
const gulp = require('gulp')
const cleanCSS = require('gulp-clean-css');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify-es').default;
const htmlmin = require('gulp-htmlmin');

gulp.task('minify-css', () => {
  return gulp.src('src/css/*.css')
    .pipe(rename("snake.min.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(cleanCSS({debug: true}, (details) => {
        console.log(`${details.name}: ${details.stats.originalSize}`);
        console.log(`${details.name}: ${details.stats.minifiedSize}`);
      }))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task("uglify-js", function () {
    return gulp.src("src/js/app.js")
        .pipe(rename("bundle.min.js"))
        .pipe(uglify(/* options */))
        .pipe(gulp.dest("dist/js/"));
});

gulp.task('minify-html', function() {
    return gulp.src('src/index.html')
      .pipe(htmlmin({collapseWhitespace: true}))
      .pipe(gulp.dest('dist'));
     
  });

gulp.task('watch', function() {
   
    gulp.watch('src/js/*.js', ['uglify-js']);
    gulp.watch('src/css/*.css', ['minify-css']);
    gulp.watch('src/*.html', ['minify-html']);
});
