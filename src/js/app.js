const cvs = document.getElementById('snake');
const ctx = cvs.getContext('2d');
const cvsW = cvs.clientWidth,
    cvsH = cvs.clientHeight;
const blockSize = 10;
let score = 0;
let snake = [];
let direction = "right";
let input = "right";
let loop;
( Snake = () => {
    let paintCell = (x, y) => {
		ctx.fillStyle = "blue";
		ctx.fillRect(x*blockSize, y*blockSize, blockSize, blockSize);
		ctx.strokeStyle = "white";
		ctx.strokeRect(x*blockSize, y*blockSize, blockSize, blockSize);
	}
    
   let generateSnake = (startingLength) => {
       for(var i = startingLength-1; i>=0; i--) {
           //This will create a horizontal snake starting from the top left
           snake.push({x: i, y:0});
       }
   }
   let makeFood = () => {

   }
   let moveSnake = () => {
    let nx = snake[0].x;
    let ny = snake[0].y;
    
    if(nx == cvsW / blockSize || ny == cvsH / blockSize || ny == -1 || nx == -1){
      init();
        return;
    }else{
    switch(input) {
        case "right":
        nx++;
        break;
        case "down":
        ny++;
        break;
        case "left":
        nx--;
        break;
        case "up":
        ny--;
    }
    direction = input;
   let tail = snake.pop();
    tail.x = nx;
    tail.y = ny;
    snake.unshift(tail);
    console.log(input);
    for(let j = 0; j < snake.length; j++){
        let s = snake[j];
        let snakeY = s.y;
        let snakeX = s.x;
        paintCell(snakeX, snakeY);           
    } 
   }

   
   }
    let paint = (dir) => {      
        ctx.fillStyle = "white";
		ctx.fillRect(0, 0, cvsW, cvsH);
		ctx.strokeStyle = "black";
		ctx.strokeRect(0, 0, cvsW, cvsH);      
        moveSnake();
    }
   
   let init = () => {
    window.addEventListener('keydown', (evt) => {
        switch(evt.keyCode) {
            case 37:
                if(direction !== "right") input = "left";
            break;
            case 38:
            if(direction !== "down")input = "up";      
            break;
            case 39:
            if(direction !== "left")input = "right";
            break;
            case 40:
            if(direction !== "up")input = "down";
            break;
        }
    });
    generateSnake(5);
    setInterval(paint, 55);
  
   }
   init();
})();






